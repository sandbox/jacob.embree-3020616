<?php

/**
 * @file
 * Defining the API part of the message UI module.
 */

/**
 * Implements hook_message_ui_view_alter().
 *
 * @param $build
 *  A render-able array which returned by the page callback function.
 * @param $message
 *  The message object.
 */
function hook_message_ui_view_alter(&$build, $message) {
  // Check the output of the message as you wish.
}

/**
 * Control access to a message.
 *
 * Modules may implement this hook if they want to have a say in whether or not
 * a given user has access to perform a given operation on a message.
 *
 * The administrative account (user ID #1) always passes any access check, so
 * this hook is not called in that case. Users with the "bypass message access
 * control" permission may always view and edit messages through the
 * administrative interface.
 *
 * Note that not all modules will want to influence access on all message types.
 * If * your module does not want to actively grant or block access, return NULL
 * or simply return nothing. Blindly returning FALSE will break other message
 * access modules.
 *
 * Also note that this function isn't called for message listings (e.g., RSS
 * feeds, a recent messages block, etc.).
 *
 * @param $message
 *   Either a message object or the machine name of the message type on which to
 *   perform the access check.
 * @param $op
 *   The operation to be performed. Possible values:
 *   - "create"
 *   - "delete"
 *   - "edit"
 *   - "view"
 * @param $account
 *   The user object to perform the access check operation on.
 *
 * @return
 *   - MESSAGE_UI_ALLOW: if the operation is to be allowed.
 *   - MESSAGE_UI_DENY: if the operation is to be denied.
 *   - NULL: to not affect this operation at all.
 */
function hook_message_message_ui_access_control($message, $op, $account) {
  $type = is_string($message) ? $message : $message->type;

  if (in_array($type, array_keys(message_ui_get_types()))) {
    if (user_access($op . ' any message instance', $account) || user_access($operation . ' a ' . $type . ' message instance', $account)) {
      return MESSAGE_UI_ALLOW;
    }
  }
}
